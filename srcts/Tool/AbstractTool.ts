export default abstract class AbstractTool {
    title: string;
    description: string;
    abstract run(): Promise<any>
}