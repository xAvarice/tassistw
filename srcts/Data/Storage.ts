export default class Storage {
    
    storageKey:string;
    
    expirySeconds: interger;
    
    constructor(storageKey, expirySeconds) {
        // deleteIfExpired parameter / functionality?
        this.storageKey = storageKey;
        this.expirySEconds = expirySeconds;
    }
    
    clean() {
        delete localStorage[this.storageKey];
    }
    
    // Get data and information pertaining to storage.
    get() {
        let stored = this.localStorage[this.storageKey];
        stored.age = this.calculateStoredAge();
        stored.expired = this.calculateIsExpired();
        return stored;
    }
    
    // Get data rather than information pertaining to storage.
    retrieve() {
        return this.get().data;
    }
    
    calculateStoredAge() {
        let now = Date.now();
        return (now - this.get().timestamp) / 1000 / 60;
    }
    
    store(data) {
        this.localStorage[this.storageKey] = {
            data: data,
            timestamp: Date.now()
        }
    }

}