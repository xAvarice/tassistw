import * as underscore from 'underscore'
import Storage from 'Data/Storage'

export default class Collection {

    storage: Storage
    
    items:any[];

    constructor(storageKey) {
        // Add arguments for cache time?
        this.storage = new Storage(storageKey);
    }
    
    setItems(items) {
        this.items = items;
    }
    
    // Prioritise collection data to storage
    // Switch between getting items from items array and storage?
    // Use retrieve if no items set here?
    getItems() { return this.items }

    getItemsByFields(selectObject: any) {
        return _.where(this.getItems(), selectObject);
    }

    getItemsByField(field, value) {
        // Is there a way to do this in a single line?
        let select = {};
        select[field] = value;
        return _.where(this.getItems(), select);
    }

}