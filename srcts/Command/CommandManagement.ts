import CommandQueue from 'Command/CommandQueue';

export default class CommandManagement {
    public commandQueue: CommandQueue

    constructor() {
        this.commandQueue = new CommandQueue;
    }
}