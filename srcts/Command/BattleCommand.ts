import Command from 'Command/Command'

export default class BattleCommand extends Command {
    units:Unit[] = {};
    setUnitQuantity(unitKey, quantity) { this.units[unitKey].quantity = quantity };
    getUnit(unitKey) { return this.units[unitKey]; }
}

interface Unit {
    name: string;
    quantity: integer;
}