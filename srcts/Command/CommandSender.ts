import Command from 'Command/Command'
import Attack from 'Command/Attack'
import Support from 'Command/Support'
import Trade from 'Command/Trade'

export default class CommandSender {
    this.commandType = 'attack';
    
    // Will Command not match trade/attack/support classes even though they extend from it?
    send(command: Command) {
        let type = command.commandType
        this[type](command);
    }
    
    trade(command: Trade) {
    }
    
    attack(command: Attack) {
    }
    
    support(command: Support) {
    }
}