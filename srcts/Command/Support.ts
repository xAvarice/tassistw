import BattleCommand from 'Command/BattleCommand'

export default class Support extends BattleCommand {
    this.commandType = 'support';
}