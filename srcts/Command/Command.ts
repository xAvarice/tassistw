import CommandSender from 'Command/CommandSender'

export default class Command {
    commandType: string;
    
    // Source and target take villages and sender worries about what data is available?
    source = {
        id: null,
        x: null,
        y: null
    }
			
    target = {
        id: null,
        x: null,
        y: null
    }
    		
    send() {
        CommandSender.send(this))
    }
}