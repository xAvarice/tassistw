import Command from 'Command/Command';

export default class CommandQueue {
    millisecondsGap: integer = 1000;
    isStopFlagSet: boolean = false;
    
    queue: Command[]
    
    stop() { 
        this.stopFlag = true;
        window.clearInterval(this.queueIntervalId);
    }
    
    start() {
        this.processNext().done(() => {
            if (!this.isStopFlagSet) {
                setTimeout(this.start, this.millisecondsGap);
            }
        })
    }
    
    process(command) {
        return command.send()
    }
    
    processNext() { 
        return new Promise((resolve, reject) => {
            this.process(this.queue[0]).done(result => {
                resolve(result);
            });
        });
    }

}