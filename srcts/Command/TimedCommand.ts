import Command from 'Command/Command'

export default class TimedCommand {
    command: Command;
    timed = true;

    getRemainingSeconds() {
        var remaining = parseInt((this.getSendUnixTime() - this.time.getCurrentTime()) / 1000);
        return remaining;
    }
    
    startTimingLoop() {
        console.log('Starting Fast Wait, time remaining: ' + getRemainingSeconds());
        var sent = false;
        
        while (!sent) {
            var safety = timedCommand.delay.safety;
            if (this.time.getCurrentTime() - safety > this.getSendUnixTime()) {
                sendAttack();
                sent = true;
            }
        }
    }
}