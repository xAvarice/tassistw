import * as underscore from 'underscore'
import Village from 'Village/Village'
import Tribe from Tribe/Tribe
import EncodingParser from 'Parse/EncodingParser'
import VillageDataParser from 'Parse/VillageDataParser'
import Collection from 'Data/Collection'

export default class WorldVillages {

    collection: Collection

    constructor() {
        this.collection = new Collection({ 
            key: 'worldVillages', 
            expiry: 60 * 3 // 3 minutes 
        });
    }
    
    // Ensures data is up to date before usage.
    load() {
        return new Promise((resolve, reject) => {
            // This may cause a problem due to resolving promise inside promisde rather than $.Deferred
    		$.ajax({
    			url: '/map/village.txt'
    		}).then(response => {
                let villages = VillageDataParser.parseFromApi(response);
    		})
        })
    }
    
    get() {
        this.collection.getItems();
    }

    getWithinRadius(subject: Village, radius: integer) {
        let result = [];
        
        // Parse to within pythagorean radius
        for (let i = 0; i < collection.length; i++) {
            let foreign = collection[i];
            let distance = subject.getDistanceTo(foreign);
            
            // Add village to results if within radius parameter.
            if (distance <= radius) {
                result.push(foreign);
            }
        }
    }
}