// Set up a WorldConfig parser, and an xml parser for reuse.
import WorldConfigParser from 'Parse/WorldConfigParser'

export default class WorldConfig {
    configKeys:string[] = [
        'unit_speed', 
        'game_speed'
    ];
    
    public config: WorldConfiguration
    
    load() {
    }
}

interface WorldConfiguration {
    unit_speed: integer;
    game_speed: integer;
}