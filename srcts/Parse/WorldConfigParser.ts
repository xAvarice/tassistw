import XmlParser from 'Parse/XmlParser'

export default class WorldConfigParser {
    public static parse(response) {
        let xmlData = XmlParser.getData(response);
        return xmlData;
    }
}