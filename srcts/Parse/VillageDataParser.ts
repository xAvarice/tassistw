export default class VillageDataParser {

    // '/map/village.txt'
    public static parseFromApi(response) {
		let rows = data.split('\n');
		let result = [];

		for (let i = 0; i < rows.length; i++) {
			let columns = rows[i].split(',');

			if (rows[i].trim() === '') {
				continue;
			}
    
            // Refactor to instantiate village objects? Too resource hungry?
			let village = {
				id: columns[0],
				name: EncodeParser.urlDecodeString(columns[1]),
				x: parseInt(columns[2]),
				y: parseInt(columns[3]),
				player: EncodeParser.urlDecodeString(columns[4]),
				points: columns[5]
			};

			result.push(village);
		}

		return result;
    }
}