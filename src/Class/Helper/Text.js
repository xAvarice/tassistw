define(
	function() {

	var TextHelper = {
		getCoordsFromText: function(text) {
			var regex = /\d{3}\|\d{3}/g;
			var coords = text.match(regex);
			return coords;
		}
	};

	return TextHelper;
});