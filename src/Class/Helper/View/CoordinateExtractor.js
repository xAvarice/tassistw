define(['underscore', 'Class/Helper/Text', 'text!Template/Utility/Village/coordinate-extractor.html'],
	function(_, textHelper, coordExtractTemp) {
			
	var CoordinateExtractor = {
		getCoordinatesFromText: function() {
			return $.Deferred(function() {
				var self = this;
				Dialog.show('coordsfromtext', _.template(coordExtractTemp)());
				$('#coord-village-go').on('click', function() {
					var textValue = $('#coord-village-text').val();
					var coords = textHelper.getCoordsFromText(textValue);

					if (coords.length > 0) {
						self.resolve(coords);
					} else {
						UI.ErrorMessage('No valid coordinates found in text, please try again.');
					}

				});
			});
		}
	};

	return CoordinateExtractor;
});