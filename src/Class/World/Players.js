define(['Class/Game/Request', 'Class/Game/User', 'jquery', 'jquery.cookie'], function(Request, User, Jquery) {

	var GameMap = {
		findVillagesWithin: function(radius, center) {
			this.loadVillageByCoord(5, 5);
			return true;
		},
		loadVillageByCoord: function(x, y) {

			console.log(Timing);
			console.log(Timing.getCurrentServerTime());

			var params = {
				client_time: Timing.getCurrentServerTime(), // Create own timing class
				village: Jquery.cookie('global_village_id'), // Refactor dependency
				offset: 0, // This is only due to css and pagination
				limit: 9, //  ^
				screen: 'api',
				ajax: 'target_selection',
				input: x + '|' + y,
				type: 'coord'
			};
			var request = new Request;
			request.url = '/';
			request.method = 'GET';
			request.callback = function(response) { console.log(response); }
			request.params = params; 
			request.send(function(result) {
				console.log('request result:');
				console.log(result);
			});
			// village:362 // User.getCurrentVillage();
			// screen: 'overview'
			// ajax: 'map_info'
			// source: 362 // User.getCurrentVillage();
			// village: 808
		}
	};

	return GameMap;
});