define(
	[
		'underscore', 
		'datacollection', 
		'text!Template/Utility/Village/proximity-modeller.html',
		'ko',
		'https://cdnjs.cloudflare.com/ajax/libs/PapaParse/4.3.3/papaparse.js'
	], function(_, DataCollection, proximityModellerTmpl, ko, csvParser) {

	var ProximityModeller = {
		subjectEntities: [],
		foreignEntities: [],

		MAX_DISTANCE_DESIRED: 25.391301,

		DANGER_DISTANCES: {
			IN_MOSTDANGER_DISTANCE: 5,
			IN_FRONTLINE_DISTANCE: 7,
			IN_NOBLE_DISTANCE: 25.391301,
		},

		DANGER_SCORES: {
			IN_MOSTDANGER_DISTANCE: 50,
			IN_FRONTLINE_DISTANCE: 30,
			IN_NOBLE_DISTANCE: 10,
		},

		useDialog: false,

		viewModel: null,

		statusCounts: {
			IN_MOSTDANGER_DISTANCE: 0,
			IN_FRONTLINE_DISTANCE: 0,
			IN_NOBLE_DISTANCE: 0,
			SAFE: 0,
			TOTAL: 0
		},

		updateCount: function(subject) {
			// Increment status count for one subject with said status
			this.statusCounts[subject.dangerStatus]++;
			this.statusCounts.TOTAL++;

			// View model key
			var key = (subject.dangerStatus.replace('IN_', '').replace('_DISTANCE', '').toLowerCase()) + 'Count';

			// Update view model count for specific status
			this.viewModel[key](this.statusCounts[subject.dangerStatus]);

			// Update view model total count
			this.viewModel.totalCount(this.statusCounts.TOTAL);
		},

		isCancelled: false,

		decodeName: function(name) {
			return window.decodeURIComponent(name).replace(/\+/g, ' ');
		},

		startDialog: function() {
			var proximityModeller = this;

			function ProximityModellerViewModel() {
			    this.message = "Modelling Promixity:";
			    this.processed = ko.observable(0);
			    this.subjectsToProcess = proximityModeller.subjectEntities.length;
			    this.safeCount = ko.observable(0);
			    this.nobleCount = ko.observable(0);
			    this.frontlineCount = ko.observable(0);
			    this.mostdangerCount = ko.observable(0);
			    this.totalCount = ko.observable(0);
			    this.processing = ko.observable(true);

			    this.getPercentComplete = function() {
			    	var percent = (this.processed() / this.subjectsToProcess) * 100;
			    	return percent.toPrecision(4);
			    };

			    this.getPercentOfTotal = function(number) {
			    	if (number() == 0) {
			    		return number();
			    	} 

			    	var rounded = (number() / this.totalCount()) * 100;
			    	return rounded.toPrecision(4);
			    };
			}

			this.viewModel = new ProximityModellerViewModel();

			return $.Deferred(function() {
				var self = this;

				setTimeout(function() {
					window.Dialog.show('ProximityModellerDialog', _.template(proximityModellerTmpl)());
					ko.applyBindings(proximityModeller.viewModel);

					$('#proximity-modeller-cancel').on('click', function() {
						proximityModeller.isCancelled = true;

						$('#proximity-modeller-cancel').off('click');
						$('#proximity-modeller-cancel').on('click', function() {
							Dialog.close();
						});
					});

					setTimeout(function() {
						self.resolve();
					}, 2000);

				}, 1000);
			});
		},

		run: function(allyVillages, enemyVillages, showDialog = false, playerInfo = null) {
			this.setup(allyVillages, enemyVillages, showDialog, playerInfo).then(function() {
				ProximityModeller.processQueue();
			});
		},

		startSave: function() {
			this.download(csvParser.unparse(JSON.stringify(this.getReadableResults())));

			// Change text and behaviour of done button.
			$('#proximity-modeller-done').off('click');
			$('#proximity-modeller-done').html('Close');
			$('#proximity-modeller-done').on('click', function() {
				Dialog.close();
			});
		},

		getReadableSubjectOwner: function(subject) {
			var player = _.find(this.playerInfo, function(player) { return player.id === subject.player; });

			if (player) {
				return this.decodeName(player.name);
			}

			return false;
		},

		getReadableResults: function() {
			var results = this.results;
			var readableResults = [];

			for (var i = 0; i < results.length; i++) {
				var current = results[i];
				var readable = {
					name: this.decodeName(current.name),
					coordinate: current.x + '|' + current.y,
					points: current.points,
					status: current.dangerStatus,
					score: current.dangerScore
				};

				if (readableOwnerName = this.getReadableSubjectOwner(current)) {
					readable.owner = readableOwnerName;
				}

				readableResults.push(readable);
			}

			return readableResults;
		},

		download: function(data) {
			var filename = 'Proximity Model TW.csv';
	        var blob = new Blob([data], { type: 'text/csv;charset=utf-8;' });
	        if (navigator.msSaveBlob) { // IE 10+
	            navigator.msSaveBlob(blob, filename);
	        } else {
	            var link = document.createElement("a");
	            if (link.download !== undefined) { // feature detection
	                // Browsers that support HTML5 download attribute
	                var url = URL.createObjectURL(blob);
	                link.setAttribute("href", url);
	                link.setAttribute("download", filename);
	                link.style.visibility = 'hidden';
	                document.body.appendChild(link);
	                link.click();
	                document.body.removeChild(link);
	            }
	        }

		},

		setup: function(subjectEntities, foreignEntities, dialog = false, playerInfo = null) {

			// Store village groups locally.
			this.subjectEntities = subjectEntities;
			this.foreignEntities = foreignEntities;

			// Assign new copy of subject entities, so original parameter is not modified.
			this.queue = subjectEntities.slice(0);

			this.playerInfo = playerInfo;

			// Set parent object deferred for later access and completion callbacks.
			this.promise = $.Deferred();
			this.generateOptimisedForeignEntityCollection();

			if (dialog) {
				this.useDialog = true;
				return this.startDialog();
			}

			return $.Deferred().resolve().promise();
		},

		queue: [],

		optimizedForeignEntityCollection: null,

		generateOptimisedForeignEntityCollection: function() {
			var collection = new DataCollection(this.foreignEntities);

			// Get lowest and highest of both coordinates
			var maxX = _.max(this.subjectEntities, function(o) { return o.x; }).x + this.MAX_DISTANCE_DESIRED;
			var maxY = _.max(this.subjectEntities, function(o) { return o.y; }).y + this.MAX_DISTANCE_DESIRED;
			var lowX = _.min(this.subjectEntities, function(o) { return o.x; }).x - this.MAX_DISTANCE_DESIRED;
			var lowY = _.min(this.subjectEntities, function(o) { return o.y; }).y - this.MAX_DISTANCE_DESIRED;

			collection.query().filter({ x__lt: lowX }).remove();
			collection.query().filter({ x__gt: maxX }).remove();
			collection.query().filter({ y__lt: lowY }).remove();
			collection.query().filter({ y__gt: maxY }).remove();

			this.optimizedForeignEntityCollection = collection;
		},

		// Ensures data is not removed from stored optimized collection.
		getOptimizedForeignEntityCollection: function() {
			var values = this.optimizedForeignEntityCollection.query().values();
			var collection = new DataCollection(values);
			return collection;
		},

		results: [],

		processedCount: 0,

		getQueueEmpty: function() {
			return (this.queue.length === 0);
		},

		promise: null,

		complete: function() {
			var self = this;
			// Turn off processing
			this.viewModel.processing(false);
			this.promise.resolve(this);

			// Add event listener to download button which was hidden before.
			$('#proximity-modeller-done').on('click', function() {
				self.startSave();
			});

			$('#proximity-modeller-forum').on('click', function() {
				self.postForum();
			});

		},

		postForum: function() {
			// Post to forum
			// convertProxMapperCsv
		},

		processQueue: function() {
			var self = this;
			this.processNext().done(function() {

				// If queue not completed, continue processing.
				if (!self.getQueueEmpty() && !self.isCancelled) {
					self.processQueue();

				// If queue empty, resolve parent promise.
				} else {
					self.complete();
				}
			});

			return this.promise;
		},

		// Get the possible range of numbers for simpler tests.
		getRange: function(subjectDistanveValue, distance) {
			var startValue = subjectDistanveValue - distance;
			var endValue = startValue + (distance * 2);
			return _.range(startValue, endValue + 1);
		},
 
		getRadiusRangeObject: function(subject, radius) {
			var xRange = this.getRange(subject.x, radius);
			var yRange = this.getRange(subject.y, radius);
			return {
				x: xRange,
				y: yRange
			};
		},

		getRadiusRangeObjectPythagoras: function(subject, radius) {
			var startX = subject.x - radius
			var startY = subject.x - radius
			var startX = subject.x - radius
			var endX = subject.x - radius
		},

		getForeignEntityCollection: function(unoptimised = false) {
			if (unoptimised) {
				collection = this.createNewNeighbourCollection();
			}

			return this.getOptimizedForeignEntityCollection();
		},

		getFurthestForeignEntitiesCollection: function(subject) {
			var collection = this.getForeignEntityCollection();
			return this.shrinkCollectionToSquare(collection, subject, this.MAX_DISTANCE_DESIRED);
		},

		createNewNeighbourCollection: function() {
			var collection = new DataCollection(this.foreignEntities);
			return collection;
		},

		getNeighbourCollectionBySquare: function(subject, radius) {
			var collection = this.createNewNeighbourCollection();
			return this.shrinkCollectionToSquare(collection, subject, radius);
		},

		shrinkCollectionToSquare: function(collection, subject, radius) {
			var range = this.getRadiusRangeObject(subject, radius);
			collection.query().filter({ x__not_in: range.x }).remove();
			collection.query().filter({ y__not_in: range.y }).remove();
			return collection;
		},

		filterCollectionWithinSquare: function(collection, range) {
			return collection.query().filter({ x__in: range.x, y__in: range.y });
		},

		getDangerDistances: function() {
			// Get danger distances sorted by distance (lowest first)
			return this.DANGER_DISTANCES
		},

		getDistance: function(subject, foreign) {
			var xCoordinatePart = Math.pow(subject.x - foreign.x, 2);
			var yCoordinatePart = Math.pow(subject.y - foreign.y, 2);
			var distance = Math.sqrt(xCoordinatePart + yCoordinatePart);

			// Round to be consistent with tribal wars fields
			// var rounded = Math.floor(distance);
			// return rounded;
			return distance;
		},

		getVillagesWithinRadius: function(subject, radius) {
			var result = [];

			// Load optimised collection
			var foreignVillages = this.getForeignEntityCollectionWithinSquare(subject, Math.ceil(radius)).query().values();

			// Parse to within pythagorean radius
			for (var i = 0; i < foreignVillages.length; i++) {
				var foreign = foreignVillages[i];
				var distance = this.getDistance(subject, foreign);

				// Add village to results if within radius parameter.
				if (distance <= radius) {
					result.push(foreign);
				}
			}

			return result;
		},


		getForeignEntityCollectionWithinSquare: function(subject, distance) {
			return this.shrinkCollectionToSquare(
					this.getForeignEntityCollection(subject),
					subject,
					distance
				);
		},

		compare: function(subject) {
			var self = this;

			// Initialise danger properties.
			subject.dangerStatus = null;
			subject.dangerScore = 0;

			// Get max distance collection to test if village is safe.
			var maxDistanceVillages = this.getVillagesWithinRadius(subject, this.MAX_DISTANCE_DESIRED);

			// If values within max distance, danger status / score needs analysing.
			if (maxDistanceVillages.length > 0) {

				var dangerDistances = this.getDangerDistances();


				// For each criteria, perform proximity testing.
				_.each(self.DANGER_DISTANCES, function(distance, name) {
					var criteriaVillages = self.getVillagesWithinRadius(subject, distance);

					// Set danger status to first danger status with result (should be sorted by distance, lowest first)
					if (criteriaVillages.length > 0 && subject.dangerStatus === null) {
						subject.dangerStatus = name;
					}

					subject.dangerScore += criteriaVillages.length * self.DANGER_SCORES[name];
				});


			// Village is safe (within max distance defined)
			} else {
				subject.dangerStatus = 'SAFE';
				subject.dangerScore = 0;
			}

			return subject;
		},

		processSubject: function(subject) {
			var result = this.compare(subject);
			return result;
		},

		processResult: function(result) {
			this.results.push(result);
			this.processedCount++;
			this.updateCount(result);
			this.viewModel.processed(this.processedCount);
		},

		processNext: function() {
			var self = this;
			return $.Deferred(function() {
				var currentProcess = this;

				if (!self.getQueueEmpty()) {
					var subject = self.queue[0];
					var result = self.processSubject(subject);

					// Store result
					self.processResult(result);

					// remove from queue
					self.queue.shift();
				}

				setTimeout(function() {
					// Finished processing queue item.
					currentProcess.resolve();
				}, 150);

			});
		},
	};

	return ProximityModeller;
});