define(
	[
		'underscore', 
		'Class/Game/Village/Village', 
		'Class/Tribe/TribeMembers',
		'Class/Parse/EncodingParser'
	], 
	function(_, Village, TribeMembers, EncodeParser) {

	var WorldVillages = {
		store: function(villages, storeAsObject = true) {
			localStorage.lmfWorldVillages = JSON.stringify(villages);
			localStorage.lmfWorldVillagesAge = Date.now() / 1000;

			if (storeAsObject) {
				this.setVillageListObject(villages);
			}
		},


		tribeMembers: TribeMembers,

		setCollection: function(villages) {
			this.collection = villages;
		},

		collection: null,

		get: function() {
			var self = this;
			return $.Deferred(function() {
				var deferred = this;
				self.load().done(function() {
					self.getCollection().done(function(collection) {
						deferred.resolve(collection);
					});
				});
			});
		},

		getCollection: function() {
			var self = this;
			return $.Deferred(function() {
				var deferred = this;

				self.load().done(function() {
					deferred.resolve(self.getFromStorage());
				});
			});
		},

		loadTribeMembers: function() {
			var self = this;
			return $.Deferred(function() {
				var promise = this;
				self.tribeMembers.load().then(function() {
					promise.resolve(self);
				});
			});
		},

		villageObject: {},

		getVillageListObject: function() {
			return this.villageObject;
		},

		setVillageListObject: function(data) {
			for (var i = 0; i < data.length - 1; i++) {
				var village = data[i];
				this.villageObject[village.x + '|' + village.y] = village;
			}
			return this;
		},

		getVillagesByTribeIds: function(tribeIds) {
			var allyPlayers = tribePlayerManager.getPlayersByTribeIds(allyTribeIds);
			var allyPlayerIds = _.pluck( allyPlayers, 'id');
			var allyVillages = villageManager.getVillagesByPlayerIds(allyPlayerIds);
		},

		getVillagesByCoordStrings: function(coordStringArray) {
			var result = [];

			for (var i = 0; i < coordStringArray.length; i++) {
				var village = this.getVillageByCoordString(coordStringArray[i]);
				if (village) {
					result.push(village);
				}
			}

			return result;
		},

		getVillageByCoordString: function(coordString) {
			var stored = this.getFromStorage();
			var coordStringParts = coordString.split('|');
			var select = { x: parseInt(coordStringParts[0]), y: parseInt(coordStringParts[1]) };
			var result = _.first(_.where(stored, select));

			if (result.length === 0) {
				result = false;
			}

			return result;
		},

		getVillagesByPlayerIds: function(playerIds) {
			var method = function(village) { 
				var testResult = _.contains(playerIds, village.player);
				return testResult;
			};
			var villages = this.getFromStorage();
			var result = _.filter(villages, method);
			return result;
		},

		getVillagesByFields: function(select) {
			var stored = this.getFromStorage();
			return _.where(stored, select);
		},

		getVillagesByField: function(field, value) {
			var stored = this.getFromStorage();
			var select = {};
			select[field] = value;
			return _.where(stored, select);
		},

		getDistance: function(subject, foreign) {
			var xCoordinatePart = Math.pow(subject.x - foreign.x, 2);
			var yCoordinatePart = Math.pow(subject.y - foreign.y, 2);
			var distance = Math.sqrt(xCoordinatePart + yCoordinatePart);
			return distance;
		},

		getVillagesWithinRadius: function(subject, radius) {
			var result = [];

			// Load optimised collection
			var collection = this.getFromStorage();

			// Parse to within pythagorean radius
			for (var i = 0; i < collection.length; i++) {
				var foreign = collection[i];
				var distance = this.getDistance(subject, foreign);

				// Add village to results if within radius parameter.
				if (distance <= radius) {
					result.push(foreign);
				}
			}

			return result;
		},


		// Return age of store in minutes
		getStorageAge: function() {
			var age = false;

			if (localStorage.lmfWorldVillages) {
				var now = Date.now() / 1000;
				age = (now - localStorage.lmfWorldVillagesAge) / 60;
			}

			return age;
		},
		parseVillageData: function(data) {
			var rows = data.split('\n');
			var result = [];

			for (var i = 0; i < rows.length; i++) {
				var columns = rows[i].split(',');

				if (rows[i].trim() === '') {
					continue;
				}

				var village = {
					id: columns[0],
					name: EncodeParser.urlDecodeString(columns[1]),
					x: parseInt(columns[2]),
					y: parseInt(columns[3]),
					player: EncodeParser.urlDecodeString(columns[4]),
					points: columns[5]
				};

				result.push(village);
			}

			return result;
		},
		load: function() {
			var self = this;
			return $.Deferred(function() {
				var villages = self.getFromStorage();
				var deferred = this;

				if (villages) {
					this.resolve(self);

				} else {
					self.getFresh().done(function(response) {
						villages = self.parseVillageData(response);
						self.store(villages);
						deferred.resolve(self);
					});
				}
			});
		},
		clean: function() {
			delete localStorage.lmfWorldVillages;
			delete localStorage.lmfWorldVillagesAge;
		},
		getFromStorage: function() {
			if (this.collection !== null) {
				return this.collection;
			}

			var age = this.getStorageAge()
			if (age && age < 10) {
				var stored = JSON.parse(localStorage.lmfWorldVillages);
				return _.map(stored, function(village) {

					village.x = parseInt(village.x);
					village.y = parseInt(village.y);
					return village;
				});
			} else {
				this.clean();
			}

			return false;
		},
		getFresh: function() {
			return $.ajax({
				url: '/map/village.txt'
			});
		}
	};

	return WorldVillages;
});