define(['Class/Data/Helper/DateTimeHelper'], function(DateTimeHelper) {

	var NoteParser = {
		parse: function(noteElement, villageData) {
			return this._parseNote(noteElement, villageData);
		},
		_getNoteDate: function(noteHtml) {
			var timeString = this._parseNoteTimeString(noteHtml);
			var noteDateObj = DateTimeHelper.formatTimeString(timeString);
			var noteDate = DateTimeHelper.formattedToMoment(noteDateObj);
			return noteDate;
		},
		parseNotes: function(html, villageData) {
			var self = this;
			var notes = [];

			var noteElements = $(html).find('.village-note');
			$.each(noteElements, function(index, noteElement) {
				if ($(noteElement).css('display') !== 'none') {
					notes.push(self.parse(noteElement, villageData));
				}
			});

			return notes;
		},
		_interpretNoteAge: function(timeString) {
			var noteDateObj = DateTimeHelper.formatTimeString(timeString);
			var noteDate = DateTimeHelper.formattedToMoment(noteDateObj);
			var ageDays = noteDate.fromNow();
			return ageDays;
		},
		_parseNoteTimeString: function(noteHtml) {
			var timeString = $(noteHtml).find('.village-note-time').text().trim();

			if (!timeString) {
				timeString = $(noteHtml).find('.village-note-head').text().trim();
			}

			return timeString;
		},
		_parseNoteAge: function(noteHtml) {
			var timeString = this._parseNoteTimeString(noteHtml);

			// DateTimeHelper
			return this._interpretNoteAge(timeString);
		},
		_parseNoteAuthor: function(noteHtml) {
			var	author = $(noteHtml).find('.village-note-head a').text();

			if (!author) {
				author = window.game_data.player.name;
			}

			return author;
		},
		_parseNoteUnixTime: function(noteHtml) {
			var date = this._getNoteDate(noteHtml);
			return date.unix();
		},
		_parseNoteContent: function(noteHtml) {
			return $(noteHtml).find('.village-note-body').text().trim();
		},
		sort: function(notes) {
			return _.sortBy(notes, function(note) {
				var age = note.age === 'N/A' ? 9999 : parseInt(note.age);
				return age;
			});
		},
		getEmptyNote: function(village) {
			return {
				unixtime: 'N/A',
				content: 'No Intel',
				author: 'N/A',
				coords: village.x + '|' + village.y,
				id: village.id,
				points: village.points,
				age: 'N/A',
				name: village.name,
				valid: false
			}
		},
		_parseNote: function(noteElement, villageData) {
			var self = this;
			var note = {
				content: self._parseNoteContent(noteElement).substring(0, 100),
				author: self._parseNoteAuthor(noteElement),
				coords: villageData.coords,
				id: villageData.id,
				points: villageData.points,
				age: self._parseNoteAge(noteElement),
				unixtime: self._parseNoteUnixTime(noteElement),
				name: villageData.name,
				valid: true
			};

			var testTime = this._parseNoteTimeString(noteElement);

			return note;
		}
	};

	return NoteParser;
});