define(function() {

	var UrlParser = {
		param: function(url, paramName) {
			var regex = new RegExp('[?&]' + paramName + '=([^&]+)');
			var found = regex.exec(url);
			return found ? found[1] : null;
		}
	}

	return UrlParser;
});