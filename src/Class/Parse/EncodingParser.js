define(
	function() {

		var EncodingParser = {
			urlDecodeString: function(string) {
				var decoded = '';
				var stripped = string.replace(/\+/g, ' ')
				decoded = decodeURIComponent(stripped);
				return decoded;
			}
		};

		return EncodingParser;
	}
);