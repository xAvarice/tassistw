define(
	['Class/Parse/UrlParser'], 
	function(UrlParser) {

	var RankingParser = {

		parse: function(page) {
			var results = [];
			var rows = $(page).find('#player_ranking_table tr:not(:first-child)');

			for (var i = 0; i < rows.length; i++) {
				var row = rows[i];
				var parsedRow = this.parseRow(row);

				if (parsedRow) {
					results.push(parsedRow);
				}
			}

			return results;
		},

		parseRow: function(row) {
			var self = this;
			var player = {
				rank: $(row).find('td').eq(0).text(),
				id: UrlParser.param($(row).find('td').eq(1).find('a').prop('href'), 'id'),
				name: $(row).find('td').eq(1).text().trim(),
				tribe: $(row).find('td').eq(2).text().trim(),
				tribeId: UrlParser.param($(row).find('td').eq(2).find('a').prop('href'), 'id'),
				points: parseInt($(row).find('td').eq(3).text()),
				villageCount:  parseInt($(row).find('td').eq(4).text()),
				villagePointsAverage: parseInt($(row).find('td').eq(5).text())
			}

			return player;
		}
	}

	return RankingParser;
});