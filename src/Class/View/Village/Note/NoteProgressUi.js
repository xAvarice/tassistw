define(
	[
		'underscore', 
		'ko',
		'text!Template/Village/village-note-progress.html',
		'text!Template/Village/village-note-forum-generator.html',
		'https://cdnjs.cloudflare.com/ajax/libs/PapaParse/4.3.3/papaparse.js',
		'Class/Tribe/Forum/ForumManagement',
		'Class/Parse/Village/NoteParser',
		'lodash'
	], 
	function(
		_,
		ko,
		NoteProgressUiTmpl,
		NoteForumPostTmpl,
		csvParser,
		ForumManagement,
		NoteParser,
		lodash
	) {

	var NoteProgressUi = {
		viewModel: null,
		viewModelConstructor: function(player) {
			this.player = player;
			this.doneCount = ko.observable(0);
			this.noteCount = ko.observable(0);
			this.total = ko.observable(0);
			this.status = ko.observable('Collecting');
			this.notes = ko.observableArray();
			this.toForum = ko.observable(false);
			this.forumTitle = ko.observable(player.name + ' Village Intel');
		},
		threadCount: null,
		start: function(player) {
			this.viewModel = new this.viewModelConstructor(player);
			this.viewModel.NoteProgress = this;
			window.Dialog.show('NoteProgressUi', _.template(NoteProgressUiTmpl)());
			ko.applyBindings(this.viewModel);
			this.viewModel.total(player.villages.length);
		},
		finish: function() {
			this.viewModel.status('Finished');
			var notes = this.viewModel.notes();
			this.viewModel.notes(NoteParser.sort(notes));
		},
		__increment: function(property) {
			property(property() + 1);
		},
		addNote: function(note) {
			this.viewModel.notes.push(note);
			this.__increment(this.viewModel.noteCount);
		},
		update: function(note) {
			this.addNote(note);
			this.__increment(this.viewModel.doneCount);

			if (this.viewModel.doneCount() === this.viewModel.total()) {
				this.finish();
			}
		},
		downloadCsv: function(context) {
			var notes = context.NoteProgress.viewModel.notes();
			var data = csvParser.unparse(JSON.stringify(notes));
			var filename = context.NoteProgress.viewModel.player.name + ' Village Notes.csv';
	        var blob = new Blob([data], { type: 'text/csv;charset=utf-8;' });
	        if (navigator.msSaveBlob) { // IE 10+
	            navigator.msSaveBlob(blob, filename);
	        } else {
	            var link = document.createElement("a");
	            if (link.download !== undefined) { // feature detection
	                // Browsers that support HTML5 download attribute
	                var url = URL.createObjectURL(blob);
	                link.setAttribute("href", url);
	                link.setAttribute("download", filename);
	                link.style.visibility = 'hidden';
	                document.body.appendChild(link);
	                link.click();
	                document.body.removeChild(link);
	            }
	        }
		},
		generatePost: function(notesChunk) {
			var postContent = _.template(NoteForumPostTmpl)({ notes: notesChunk });
			return postContent;
		},
		generatePosts: function(notes) {
			var chunks = lodash.chunk(notes, 75);
			var posts = [];

			for (var i = 0; i < chunks.length; i++) {
				posts.push(this.generatePost(chunks[i]));
			}

			return posts;
		},
		postForum: function(context) {
			var viewData = context.NoteProgress.viewModel;
			var title = viewData.forumTitle();
			var player = viewData.player.name;
			var introPost = 'Collected village intel for ' + player + '.';
			var notes = viewData.notes();
			var posts = context.NoteProgress.generatePosts(notes);
			this.threadCount = posts.length;

			ForumManagement.newThread(null, title, introPost).done(function(thread) {
				ForumManagement.addPostsToThread(
					posts,
					thread.forumId,
					thread.threadId
				).done(function() {
					var successMessage = 'Successfully posted to forum.';
					// window.UI.SuccessMessage(successMessage);
					alert(successMessage);
				});
			});
			// For each chunk
		}
	};

	return NoteProgressUi;
});