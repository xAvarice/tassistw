define(function() {
	var Window = {
		getWidth: function() {
			return $(window).width();
		},
		getHeight: function() {
			return $(window).height();
		},
		getCenterObject: function () {
			var center = { 
				x: (this.getWidth() / 2), 
				y: (this.getHeight() / 2) 
			};
			return center;
		}
	};

	return Window;
});