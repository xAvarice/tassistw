define(
	[
		'Class/Data/Helper/DateHelper',
		'Class/Data/Helper/TimeHelper',
		'moment'
	],
	function(
		DateHelper,
		TimeHelper,
		moment
	) {
		/** Class used to handle localStorage amongst other classes. */
		var DateTimeHelper = $.extend(DateHelper, TimeHelper, {
			__getCurrentServerTime: function() {
				return window.Timing.getCurrentServerTime();
			},
			__getCurrentServerTimeUTC: function() {
				return window.Timing.getCurrentServerTime() + this.getUTCOffset();
			},
			getUTCOffset: function() {
				return window.server_utc_diff;
			},
			getCurrentDate: function() {
				var date = new Date(this.__getCurrentServerTime());
				return date;
			},
			toMoment: function(date) {
				return moment(date);
			},
			getCurrentDateUtc: function() {
				var date = new Date(this.__getCurrentServerTimeUTC());
				return date;
			},
			formattedToMoment: function(timeObject) {
				return this.toMoment(timeObject);
			},
			formatTimeString: function(dateTimeString) {
				var colloquial = (dateTimeString.indexOf('today') !== -1 || dateTimeString.indexOf('yesterday') !== 1);
				var currentDate = this.getCurrentDate();

				// today at 02:59:38
				var format = { 
					years: currentDate.getFullYear(), 
					months: currentDate.getMonth(), 
					date: currentDate.getDate()
				};

				if (dateTimeString.indexOf('yesterday') !== -1) {
					// yesterday at 16:18:27
					format.date = format.date - 1;

				// Do not modify, today as current date already handles situation.
				} else if (dateTimeString.indexOf('today') === -1) {
					// 19.09.2017 at 18:44:57
					var dateParts = dateTimeString.match(/\d+\.\d+\.\d+/)[0].split('.');
					format.date = parseInt(dateParts[0]);
					format.months = parseInt(dateParts[1]) - 1;
					format.years = parseInt(dateParts[2]);
				}

				var timeParts = dateTimeString.match(/\d+\:\d+\:\d+/)[0].split(':');
				format.hours = timeParts[0];
				format.minutes = timeParts[1];
				format.seconds = timeParts[2];

				return format;
			},
			formatTimestringUtc: function(dateTimeString) {
				// today at 02:59:38
				// 19.09.2017 at 18:44:57
				// yesterday at 16:18:27
			}

		});
		return DateTimeHelper;
	}
);