define(function() {

	/** Class used to handle localStorage amongst other classes. */
	var TimeHelper = {
		getRandomSecs: function() {
			return Math.floor(Math.random() * (60 - 0 + 1)) + 0;
		},
		getRandomDelay: function(multiplierLoop = 1) {
			var randomSeconds = TimeHelper.getRandomSecs();
			var randomMs = TimeHelper.getRandomMs();
			var minimumDelay = 303;
			var maximumDelay = 956;
			minimumDelay = 303;
			maximumDelay = 556;
			var randomDelay = TimeHelper.secsToMs(randomSeconds) + randomMs;
			return Math.min(maximumDelay, Math.max(minimumDelay, randomDelay)) * multiplierLoop;
		},
		getRandomMs: function() {
			return Math.floor(Math.random() * (1000 - 0 + 1)) + 0;
		},
		getRandomMsInRange: function(min, max) {
			// apply limits
			return Math.floor(Math.random() * (max - min + 1)) + min;
		},
		secsToMs: function(seconds) {
			return seconds * 1000;
		}
	};

	return TimeHelper;
});