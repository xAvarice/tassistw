define(function() {

	/** Class used to handle localStorage amongst other classes */
	var Storage = function(storageKey = '') {
		
		this.storageKey = storageKey;

		this.get = function() {
			var storage = window.localStorage[this.storageKey]
			return storage ? JSON.parse(storage) : {};
		}

		this.set = function(value) {
			window.localStorage[this.storageKey] = JSON.stringify(value);
			return this;
		}

		this.add = function(value) {
			var data = $.merge(this.get(), value);
			return this.set(data);
		}
	};

	return Storage;
});