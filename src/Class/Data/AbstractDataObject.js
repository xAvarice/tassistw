define(function() {

	var AbstractDataObject = function(options = {}) {

		this.public = [];

		// { from: '', to: '' }
		this.fieldMap = [];

		this.get = function(field) {
			return ~this.public.indexOf(field) ? this[field] : null;
		}
 
		this.set = function(field, value) {
			if (~this.public.indexOf(field)) {
				this[field] = value;
			}

			return this;
		}

		this.getMappedFieldKey = function(fieldKey) {
			return fieldKey;
		}

		this.setData = function(data) {
			if (data !== undefined) {
				for (var field in data) {
					var fieldKey = this.getMappedFieldKey(field);
					this.set(fieldKey, data[field]);
				}
			}

			return this;
		}

		this.getData = function() {
			var data = {};
			for (var i = 0; i < this.public.length; i++) {
				var field = this.public[i];
				data[field] = this.get(field);
			}

			return data;
		}

		this.__construct = function(options) {
			this.public = options.hasOwnProperty('public') ? options.public : {};
			this.setData(options);
			return this;
		}

		this.__construct(options);
	}

	return AbstractDataObject;
});