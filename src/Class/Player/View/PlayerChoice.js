define(
	[
		'underscore', 
		'Class/Player/Player', 
		'text!Template/Player/player-choice.html', 
		'Class/Data/Storage'
	],
	function(_, Player, playerChoiceTmpl, Storage) {
			
		var PlayerChoice = function(message = '', prefkey = '') {
			this.choices = [];

			// Pref key ensures preferences are handler based.
			this.storageKey = 'lmfPlayerChoicePref' + prefkey;

			this.storage = new Storage(this.storageKey);

			this.message = message;

			this.__loadPlayerByName = function(name) {
				return $.Deferred(function() {
					var deferred = this;
					var player = new Player({ name: name });

					player.loadByName().done(function() {
						deferred.resolve(player);
					});
				});
			}

			this.__getPlayerNameInput = function() {
				var name = prompt(message);
				return name;
			}

			this.get = function() {
				// Open dialog, with prompt template.
				var name = this.__getPlayerNameInput();
				return this.__loadPlayerByName(name);
			}
		};

		return PlayerChoice;
	}
);