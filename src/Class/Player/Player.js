define(
	[
		'underscore', 
		'Class/Data/AbstractDataObject', 
		'Class/Parse/Screen/RankingPageParser',
		'Class/World/VillageCollection'
	],
	function(_, AbstractDataObject, RankParser, VillageCollection) {

		var Player = function(config = {}) {

			var self = this;

			config.public = [
				'id',
				'name',
				'points',
				'rank',
				'tribeId',
				'tribeName',
				'villageCount',
				'villages'
			];

			this.id = null;
			this.name = null;
			this.name = null;
			this.points = null;
			this.rank = null;
			this.tribeId = null;
			this.tribeName = null;
			this.villageCount = null;
			this.villages = null;

			this.load = function() {}

			this.loadVillages = function() {
				return $.Deferred(function() {
					var deferred = this;
					VillageCollection.get().done(function(villages) {
						self.villages = _.where(villages, { player: self.id + '' });
						deferred.resolve(self);
					});
				});
			}

			this.loadByName = function() {
				var self = this;
				return $.Deferred(function() {
					var deferred = this;

					$.ajax('game.php?screen=ranking&mode=player&name=' + self.name)
						.done(function(response) {
							var players = RankParser.parse(response);

							var player = _.first(_.filter(players, function(item) { 
								return item.name.toLowerCase() === self.name.toLowerCase();
							}));

							self.setData(player);

							deferred.resolve(self);
						});
				});
			}

			AbstractDataObject.call(this, config);
		};

		return Player;
	}
);