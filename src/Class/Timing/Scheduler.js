define(function() {
	var constructor = function() {
		this.interval = null;
		this.performNext = function() {}
		this.start = function() {}
		this.setInterval = function(interval) {}

		// Optional, later.
		this.repeatLast = function() {}
		this.pause = function() {}
	}

	return constructor;
});