define(['underscore', 'text!Template/Group/group-choice.html'],
	function(_, groupChoiceTmpl) {
			
	var GroupChoiceHandler = function(message = '', prefkey = '', groups = []) {
		this.choices = [];

		this.complete = function() {
			window.Dialog.close();
			this.promise.resolve(this.choices);
		}

		this.addSubmitHandler = function() {
			var self = this;
			$('#group-choice-form-done').on('click', function(event) {
				self.choices = $('#group-choice-groups').val();
				self.complete();
			});
		}

		this.promise = null,

		this.get = function() {
			var self = this;
			return $.Deferred(function() {

				// Get Input Tribes
				var choiceForm = _.template(groupChoiceTmpl);
				Dialog.show('groupChoice', choiceForm({ message: message, groups: groups }));

				self.addSubmitHandler();

				// Passed to parent so user can resolve when done
				self.promise = this;
			});
		}
	};

	return GroupChoiceHandler;
});