define(function() {

	var Group = function() {
		this.id = null;
		this.name = null;

		this.construct = function(constructObject) {

		}

		this.getIdFromUrl = function(url) {
			return url.match(/group=\d+/)[0].replace('group=', '');
		}
	}

	return Group;
});