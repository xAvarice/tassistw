define(['Class/Game/Group/Group', 'Class/Game/Village/Village'], function(Group, Village) {

	var GroupManagement = {
		loadGroups: function() {
			return new $.Deferred(function() {
				var self = this;
				this.groups = [];

				$.ajax({
					url: '/game.php?screen=overview_villages&mode=combined&group=0',
				}).then(function(response) {
					var groupElements = $(response).find('.group-menu-item:not(:first-of-type)');
					for (var i = 0; i < groupElements.length; i++) {
						var group = new Group;
						group.id = group.getIdFromUrl($(groupElements).eq(i).attr('href'));
						group.name = $(groupElements).eq(i).text();
						self.groups.push(group);
					}

					self.resolve(self.groups);
				});
			});
		},
		pagesLoaded: 0,
		getAllGroups: function() {
			return this.loadGroups();
		},
		getAllGroupIds: function() {
			return this.loadGroups();
		},
		getUnitsFromHeader: function(header) {

		},
		addVillagesToGroup: function(group, villages) {
			// load group
			// village:208
			// screen:groups
			// ajaxaction:village
			// h:70371afd
			// (empty)
			// client_time:1498596198
			// Form Data
			// view source
			// view URL encoded
			// groups[]:8933
			// groups[]:10727
			// village_id:208
			// mode:village
		},
		troopsOnPage: {

		},
		getTroopsFromGroupPageRow: function(row) {
			var troops = {};
			for (var troop in this.troopsOnPage) {
				var columnIndex = this.troopsOnPage[troop];
				var troopCell = $(row).find('td').eq(columnIndex);
				var troopCount = parseInt(troopCell.text());
				troops[troop] = troopCount;
			}

			return troops;
		},
		setPageTroops: function(response) {
			var headerCells = $(response).find('#units_table th');
			for (var i = 0; i < headerCells.length; i++) {
				var image = $(headerCells).eq(i).find('img');
				if (image.length) {
					var href = $(image).prop('src');
					var unit = href.match(/unit_[a-z]+/)[0].split('_')[1];
					this.troopsOnPage[unit] = i;
				}
			}
		},
		getVillageIdFromUrl: function(url) {
			return url.match(/village=\d+/)[0].split('=')[1];
		},
		getVillageCoordsFromText: function(coordinates) {
			return coordinates.match(/\d{3}\|\d{3}/)[0];
		},
		getVillagesFromGroupPage: function(response) {
			var villages = [];
			var rows = $(response).find('#units_table tbody');
			this.setPageTroops(response);

			for (var i = 0; i < rows.length; i++) {
				var village = new Village;
				var villageLink = $(rows).eq(i).find('td:first-child a').prop('href');
				village.id = this.getVillageIdFromUrl(villageLink);
				village.troops = this.getTroopsFromGroupPageRow($(rows).eq(i));
				var coordinates = this.getVillageCoordsFromText($(rows).eq(i).find('td:first-child a').text()).split('|');
				village.x = parseInt(coordinates[0]);
				village.y = parseInt(coordinates[1]);
				villages.push(village);
			}

			return villages;
		},
		loadGroupPageVillages: function(group, page = false) {
			var self = this;
			var params = 'screen=overview_villages&type=own_home&mode=units&group=' + group;
			if (page) {
				params + '&page=' + page;
			}

			return $.Deferred(function() {
				var deferred = this;
				$.ajax('/game.php?' + params).done(function(response) {
					deferred.resolve(self.getVillagesFromGroupPage(response));
				});
			});
		},
		getAllVillagesWithGroupIds: function(groupIds) {
			var self = this;

			return $.Deferred(function() {
				var deferred = this;
				var villages = [];
				var groupsLoaded = 0;

				for (var i = 0; i < groupIds.length; i++) {
					var groupId = groupIds[i];
					self.getVillagesWithinGroup(groupId).done(function(groupVillages) {
						villages = $.merge(villages, groupVillages);
						groupsLoaded++;

						if (groupsLoaded === groupIds.length) {
							deferred.resolve(villages);
						}
					});
				}
			});
		},
		getVillagesWithinGroup: function(group) {
			var self = this;
			return $.Deferred(function() {
				var deferred = this;
				var villages = [];
				self.getNumberGroupPages(group).done(function(numPages) {
					if (numPages === 1) {
						self.loadGroupPageVillages(group).done(function(pageVillages) {
							deferred.resolve(pageVillages);
						});

					} else {
						self.loadMultipleGroupPagesVillages(group, numPages).done(function(mutlipageVillages) {
							deferred.resolve(mutlipageVillages);
						});
					}
				});
			});
		},
		// Start getting group villages from cache.
		getIsGroupCached: function() {
			// var MAX_CACHE_AGE = 1000;
		},
		loadMultipleGroupPagesVillages: function(group, pages) {
			var self = this;
			return $.Deferred(function() {
				var deferred = this;
				var pagesLoaded = 0;
				var villages = [];

				for (var i = 0; i < pages; i++) {
					setTimeout(function() {
						self.loadGroupPageVillages(group, i + 1).done(function(pageVillages) {
							villages = $.merge(villages, pageVillages);
							pagesLoaded++;

							if (pagesLoaded === pages) {
								deferred.resolve(villages);
							}
						});
					}, 1000 * i);
				}
			});
		},
		getNumberGroupPages: function(group) {
			return $.Deferred(function() {
				var deferred = this;
				$.ajax({
					url: '/game.php?&screen=overview_villages&type=complete&mode=units&group=' + group
				}).done(function(response) {
					var pageSelect = $(response).find('#paged_view_content table select:first-of-type');

					if (pageSelect.length > 0) {
						var options = $(pageSelect).find('option'); 
						var count = options.length - 1;

						if ($(options).eq(options.length - 1).text() !== 'all') {
							count = options.length;
						}

						deferred.resolve(count);

					} else {
						deferred.resolve(1);
					}
				});
			});
		}
	}

	return GroupManagement;
});