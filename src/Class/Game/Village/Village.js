define(
	[
		'Class/Game/Village/VillageNotes',
		'Class/Data/AbstractDataObject'
	], 
	function(VillageNotes, AbstractDataObject) {

	var Village = function(config = {}) {
		var self = this;
		this.id = null;
		this.player = null;
		this.points = null;
		this.name = null;
		this.troops = null;
		this.buildings = null;
		this.x = null;
		this.y = null;

		this.collection = null;

		config.public = ['collection', 'player', 'points', 'name', 'id', 'buildings', 'notes', 'x', 'y', 'troops'];

		this.load = function(id) {
		}

		this.getNotes = function() {
			var data = $.extend(self.getData(), { coords: self.x + "|" + self.y });
			return new VillageNotes(data);
		}

		this.loadByCoordinates = function(coordinates) {
			var self = this;
			var coordinateParts = coordinates.split('|');
			var x = parseInt(coordinateParts[0]);
			var y = parseInt(coordinateParts[1]);

			return $.Deferred(function() {
				var deferred = this;
				self.collection.getCollection().done(function(collection) {
					var result = _.where(collection, { x: x, y: y });
					var village = _.first(result);

					deferred.resolve(village);
				});
			});
		}

		// refactor with Class/Parse/UrlParser
		this.getIdFromUrl = function(url) {
			return url.match(/group=\d+/)[0].replace('group=', '');
		}

		AbstractDataObject.call(this, config);
	}

	return Village;
});