define(['Class/Game/Village/Village'], function(Village) {

	var VillageManagement = {
		loadVillages: function() {
			console.log('loading villages');
			return new $.Deferred(function() {
				var self = this;
				$.ajax({
					url: '/game.php?screen=overview_villages&mode=combined&group=0',
					success: function(response) {
						self.villages = [];
						var villageElements = $(response).find('.village-menu-item:not(:first-of-type)');
						console.log(villageElements);
						for (var i = 0; i < villageElements.lengtht; i++) {
							var village = new Village;
							village.id = village.getIdFromUrl($(villageElements).eq(i).attr('href'));
							village.name = $(villageElements).eq(i).text();
							villages.push(village);
						}

						self.resolve(self.villages);
					}
				});
			});
		},
		getAllVillages: function() {
			return this.loadVillages();
		},
		getAllVillageIds: function() {
			return this.loadVillages();
		}
	}

	console.log(VillageManagement.getAllVillageIds());
	console.log(VillageManagement.getAllVillages());

	return VillageManagement;
});