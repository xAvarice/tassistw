define(
	[
		'Class/Server/ServerTime',
		'Class/Data/AbstractDataObject',
		'Class/Http/Request',
		'Class/Data/Helper/DateTimeHelper',
		'Class/Parse/Village/NoteParser',
		'text!Template/Village/village-note-forum-generator.html',
		'lodash'
	], 
	function(
		serverTime, 
		AbstractDataObject, 
		Request, 
		DateTimeHelper, 
		NoteParser,
		lodash
	) {

		var VillageNotes = function(config = {}) {
			var self = this;

			// Village ID
			this.id = null;
			this.coords = null;
			this.points = null;
			this.name = null;

			config.public = ['id', 'coords', 'points', 'name'];

			this.loadVillageOverviewHtml = function() {
				var self = this;
				return $.Deferred(function() {
					var deferred = this;
					$.ajax('/game.php?screen=info_village&id=' + self.id).done(function(response) {
						deferred.resolve(response);
					});
				});
			}

			this.set = function(note) {
				var config = {
					method: 'POST',
					data: {
						village_id: self.id,
						note: note
					},
					params: {
						screen: 'api',
						ajaxaction: 'village_note_edit',
						h: '',
						client_time: ''
					}
				};
				var request = new Request(config);
				return request.send();
			}

			this.get = function() {
				return this.getNotes();
			}

			this.getNotes = function() {
				var self = this;
				return $.Deferred(function() {
					var deferred = this;
					self.loadVillageOverviewHtml().done(function(overviewHtml) {
						deferred.resolve(NoteParser.parseNotes(overviewHtml, self.getData()));
					});
				});
			}

			this.getNoteParser = function() {
				return NoteParser;
			}

			this.getLatestNote = function() {
				var self = this;
				return $.Deferred(function() {
					var deferred = this;
					self.getNotes().done(function(notes) {
						var note = false;

						if (notes.length > 0) {
							note = _.last(_.sortBy(notes, 'unixtime'));
						}

						if (notes.length > 1) {
							var otherAuthors = [];
						}

						note.otherAuthors = otherAuthors;

						deferred.resolve(note);
					});
				});
			}

			AbstractDataObject.call(this, config);
		}

		return VillageNotes;
	}
);