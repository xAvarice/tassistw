define(['underscore', 'text!Template/Village/village-distance-choice.html'],
	function(_, distanceChoiceTmpl) {
			
	var TribeChoiceHandler = function() {
		this.promise = null;

		this.distance = null;

		this.key = 'village_distance_choice';

		this.setPreference = function(distance) {
			window.localStorage[this.key] = distance;
		};

		this.getPreference = function() {
			var preference = 40;

			if (window.localStorage[this.key]) {
				preference = parseFloat(window.localStorage[this.key]);
			}

			return preference;
		};

		this.getDistanceChoice = function() {
			var self = this;
			return $.Deferred(function() {
				Dialog.show('village-distance-choice', _.template(distanceChoiceTmpl)({ 
					suggested: self.getPreference()
				}));
				self.addSubmitHandler();
				self.promise = this;
			});
		};

		this.complete = function() {
			Dialog.close();
			this.promise.resolve(this.distance);
		};

		this.addSubmitHandler = function() {
			var self = this;
			$('#village-distance-done').on('click', function() {
				var distanceText = $('#village-distance').val();
				self.distance = parseFloat(distanceText);
				self.setPreference(self.distance);
				self.complete();
			});
		};
	};

	return TribeChoiceHandler;
});