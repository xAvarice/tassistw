define(['underscore', 'Class/Game/Village/Village', 'text!Template/Village/village-coord-choice.html'],
	function(_, Village, villageChoiceTmpl) {
			
	var TribeChoiceHandler = function() {
		this.promise = null;

		this.village = null;

		this.collection = null;

		this.getVillageChoice = function(collection) {
			var self = this;
			this.collection = collection;
			return $.Deferred(function() {
				Dialog.show('village-coord-choice', _.template(villageChoiceTmpl)({ 
					suggested: window.game_data.village.x + '|' + window.game_data.village.y
				}));
				self.addSubmitHandler();
				self.promise = this;
			});
		};

		this.complete = function() {
			this.promise.resolve(this.village);
		};

		this.addSubmitHandler = function() {
			var self = this;
			$('#village-choice-done').on('click', function() {
				var villageCoordinates = $('#village-choice').val();
				var village = new Village;
				village.construct({ collection: self.collection });
				village.loadByCoordinates(villageCoordinates).done(function(loadStatus) {
					if (loadStatus) {
						self.village = loadStatus;
						self.complete();
					} else {
						window.UI.ErrorMessage('Invalid input, please try again.');
					}
				});
			});
		};
	};

	return TribeChoiceHandler;
});