define(['underscore', 'text!Template/Troops/troops-buffer-choice.html'],
	function(_, troopBufferChoiceTmpl) {

	var TroopBufferChoice = function() {
		this.promise = null;

		this.units = null;

		this.key = 'troop_buffer_choice';

		this.availableUnits = window.game_data.units;

		this.storePreferences = function(preferences) {
			window.localStorage[this.key] = JSON.stringify(preferences);
		};

		this.getPreferencesFromStorage = function() {
			var storage = window.localStorage[this.key];

			if (storage) {
				return JSON.parse(storage);
			}

			return {};
		};

		this.getPreferences = function() {
			var preferences = this.getPreferencesFromStorage();
			var generated = {
				desired: {},
				buffer: {}
			};
			var types = {
				chosen: 'desired',
				buffered: 'buffer'
			};

			for (var i = 0; i < this.availableUnits.length; i++) {
				var troop = this.availableUnits[i];
				for (var prefType in types) {
					var amount = 0;
					if (typeof preferences[types[prefType]] !== 'undefined') {
						amount = preferences[types[prefType]][troop];
					}

					generated[types[prefType]][troop] = amount;
				}
			}

			return generated;
		};

		this.getUnitsChoice = function() {
			var self = this;
			return $.Deferred(function() {
				Dialog.show('village-coord-choice', _.template(troopBufferChoiceTmpl)({ 
					availableUnits: self.availableUnits,
					preferences: self.getPreferences()
				}));
				self.addSubmitHandler();
				self.promise = this;
			});
		};

		this.complete = function() {
			Dialog.close();
			this.promise.resolve(this.units);
		};

		this.objectifySerialized = function(serializedArray) {
			var result = {};

			for (var i = 0; i < serializedArray.length; i++) {
				var field = serializedArray[i];
				result[field.name] = parseInt(field.value);
			}

			return result;
		};

		this.addSubmitHandler = function() {
			var self = this;
			$('#troop-choice-done').on('click', function() {
				// parse input
				var troops = self.objectifySerialized($('#troops-choice').serializeArray());
				var buffer = self.objectifySerialized($('#troops-buffer-choice').serializeArray());
				var result = {
					desired: troops,
					buffer: buffer
				};

				// set result
				self.units = result;

				// store preference
				self.storePreferences(result)

				// complete
				self.complete();
			});
		};
	};

	return TroopBufferChoice;
});