define(['jquery'], function(jquery) {

	var Request = function() {
		this.url = '/game.php?';
		this.method = null;
		this.params = {};
		this.data = {};
		this.request_id = 0;
		
		this.send = function() {
			this.params.request_id = this.request_id;
			return $.ajax({
				method: this.method,
				data: this.params,
				url: this.url
			});
		}
	};

	return Request;
});