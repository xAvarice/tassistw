define(
[
	'Class/Command/AbstractCommand',
	'Class/Game/Time',
	'Class/Game/Request',
	''
], 
function(AbstractCommand, Time, Request) {
	var TimedCommand = Object.create(AbstractCommand);
	TimedCommand.init = function(params) {

	};
	TimedCommand.promptConfig = function() {

	};
	TimedCommand.time = new Time;
	TimedCommand.params = null;
	TimedCommand.data = null;
	TimedCommand.timed = false;
	TimedCommand.request = new Request;
	TimedCommand.send = function() {
		this.request.buildRequest({url: '', query: '', data:this.params});
		this.request.send();
	};
	TimedCommand.sendTime = null;
	TimedCommand.getRemainingSeconds = function() {
		var remaining = parseInt((this.getSendUnixTime() - this.time.getCurrentTime()) / 1000);
		return remaining;
	};
	TimedCommand.startTimingLoop = function() {
		console.log('Starting Fast Wait, time remaining: ' + getRemainingSeconds());
		var sent = false;

		while (!sent) {
			var safety = timedCommand.delay.safety;
			if (this.time.getCurrentTime() - safety > this.getSendUnixTime()) {
				sendAttack();
				sent = true;
			}
		}
	};

	return TimedCommand;
});