var Trader = {
	setupTrade: function(params) {

	},
	formatQuery: function(query) {
		var request = '';

		for (var prop in query) {
			request += prop + '=' + query[prop] + '&';
		}

		console.log(request);
		return request;
	},
	sendResources: function(params) {
		var query = {
			village: params.source,
			screen: 'market',
			ajaxaction: 'map_send',
			// csrf_token - global?
			// h: 'e9e4fefe',
			h: window.csrf_token,
			// What is this and how do I get it?
			client_time: Math.round(Timing.getCurrentServerTime() / 1e3)
		};

		// Does cookie need changing?

		var post = {
			target_id: params.target,
			wood: params.wood,
			stone: params.stone,
			iron: params.iron
		};

		// Param object
		/*
			{ wood, stone, iron, target, source }
		 */


		$.post({
			url: '/game.php?' + this.formatQuery(query),
			data: post,
			success: function(response) {
				console.log(response);
			}
		});
	}
};