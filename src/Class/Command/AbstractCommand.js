define(
	function() {
		var AbstractCommand = function() {
			this.timed = false;
			this.from = null;
			this.commandType = null;
			this.target = {
				id: null,
				x: null,
				y: null
			}
			this.units = {};
			this.setUnitQuantity = function(unit, quantity) {
				this.units[unit] = { name: unit, quantity: quantity };
			};
			this.getUnit = function(unit) {
				return this.units[unit];
			};
			this.availableUnits = [];
			this.getUnits = function() {
				var units = {};
				console.log('available units', this.availableUnits);
				this.availableUnits.forEach(function(unit) {
					units[unit] = this.units[unit];
				});

				return units;
			};
			this.getFormKey = function() {
				var self = this;
				var query = {
					screen: 'place',
					ajax: 'command',
					target: game_data.village.id,
					client_time: Math.ceil(Timing.getCurrentServerTime()),
					village: game_data.village.id
				};
				return $.ajax({
					method: 'GET',
					url: '/game.php?' + $.param(query),
					success: function(response) {
						var units = $(response).find('.unit_link');
						$.each(units, function(index, element) {
							var unit = $(element).data('unit');
							self.availableUnits.push(unit);
						});
						this.formKey = 10;
					}
				});
			},
			this.sendAttackPost = function(formkey) {
				var self = this;
				var data = {
					template_id: '',
					source_village: self.from,
					x: this.target.x,
					y: this.target.y,
					// ?
					input: '',
					// ?
					attack: 'l'
				};

				$.extend(data, self.getUnits());
				$.extend(data, self.formKey);

				console.log(data);
				return {};
				return $.ajax({
					method: 'POST',
					data: data
				});
			};
			this.formkey = null;
			this.refreshKey = false;
			this.send = function() {
				var self = this;

				if (self.refreshKey || !self.formKey) {
		            return this.getFormKey().done(function() {
		            	var key = this.formKey;
		            	return self.sendAttackPost(key);
		            });
				} else {
					return self.sendAttackPost(self.formkey);
				}
			}
		};

		return AbstractCommand;
	}
);