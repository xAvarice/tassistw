define(
	['Class/Command/AbstractCommand'],
	function(AbstractCommand) {

		var Attack = AbstractCommand;

		Attack.commandType = 'attack';

		return Attack;
	}
);