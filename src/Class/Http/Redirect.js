define(function() {

	var Redirect = {
		url: undefined,
		screen: undefined,
		base: '/game.php?',
		setScreen: function(screen) {
			this.screen = screen;
			return this;
		},
		setUrl: function(url) {
			this.url = url;
			return this;
		},
		setRedirect: function(url) {
			window.location.href = this.base + url
		},
		go: function() {
 			this.setRedirect(this.url);
		},
		goScreen: function() {
			this.setRedirect('screen=' + this.screen);
		}
	};

	return Redirect;
});