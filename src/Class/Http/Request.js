define(['Class/Data/AbstractDataObject'], function(AbstractDataObject) {

	var Request = function(config = {}) {
		var self = this;
		this.url = null;
		this.method = null;
		this.data = null;
		this.requestId = null;
		this.priority = null;
		this.callback = null;
		this.sent = false;
		this.query = null;
		
		config.public = ['method', 'data', 'requestId', 'priority', 'callback', 'url', 'query'];

		this.formatQuery = function(queryObj) {
			var query = '';
			var queryKeys = Object.keys(queryObj);
			var lastKey = queryKeys[queryKeys.length];

			for (var arg in queryObj) {
				query += arg + '=' + queryObj[arg];

				// If not last key add ampersand
				if (arg !== lastKey) {
					query += '&';
				}
			}

			return query;
		}

		this.send = function() {
			return $.Deferred(function() {
				var deferred = this;

				var url = self.query ? self.url + self.formatQuery(self.query) : self.url;

				$.ajax({
					url: url,
					method: self.method,
					data: self.data

				}).done(function() {
					self.sent = true;
					self.callback ? self.callback() : null;
					deferred.resolve(self);
				});
			});
		}

		AbstractDataObject.call(this, config);
	};

	return Request;
});