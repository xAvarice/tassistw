define(function() {

	var ForumManagement = {
		addPostToThread: function(post, forumId, threadId, iteration = 1) {
			var self = this;
			return $.Deferred(function() {
				var deferred = this;

				var urlParams = {
					screen: 'forum',
					screenmode: 'view_thread',
					thread_id: threadId,
					answer: 'true',
					action: 'new_post',
					forum_id: forumId,
					h: window.csrf_token
				};

				setTimeout(function() {
					var request = $.ajax({
						method: 'POST',
						url: '/game.php?' + $.param(urlParams),
						data: {
							do: 'send',
							message: post
						}
					});
					deferred.resolve(request);
				}, 3000 * iteration);
			});
		},

		addPostsToThread: function(posts, forumId, threadId) {
			var self = this;
			return $.Deferred(function() {
				var deferred = this;
				var requests = [];

				for (var i = 0; i < posts.length; i++) {
					var post = posts[i];
					requests.push(self.addPostToThread(post, forumId, threadId, i));
				}

				$.when.apply(requests).done(function() {
					deferred.resolve(self);
				});
			});
		},

		getDefaultForumId: function() {
			return $.Deferred(function() {
				var self = this;

				$.ajax({
					url: '/game.php?screen=forum'
				}).done(function(response) {
					var forumIdHref = $(response).find('#forum_box a.thread_button').eq(0).prop('href');
					var forumId = forumIdHref.match(/forum_id=\d+/)[0].split('=')[1];
					self.resolve(forumId);
				});
			});
		},

		createThread: function(forumId, title, intropost) {
			return $.Deferred(function() {
				var deferred = this;

				var params = {
					village: window.game_data.village.id,
					forum_id: forumId,
				    screen: 'forum',
				    screenmode: 'view_forum',
				    mode: 'new_thread',
				    action: 'new_thread',
				    h: window.csrf_token
				};

				$.ajax({
					url: '/game.php?' + $.param(params),
					method: 'POST',
					data: {
						do: 'send',
						subject: title,
						message: intropost
				    }

				}).done(function(response) {
					var threadHref = $(response).find('[name="del_posts"]').prop('action');
					var threadId = threadHref.match(/thread_id=\d+/)[0].split('=')[1];
					deferred.resolve(threadId);
				});
			});
		},

		newThread: function(forumId = null, title, intropost = 'New thread') {
			var self = this;
			return $.Deferred(function() {
				var deferred = this;

				if (!forumId) {
					self.getDefaultForumId().done(function(id) {
						self.createThread(id, title, intropost).done(function(threadId) { 
							deferred.resolve({ threadId: threadId, forumId: id });
						});
					});

				} else {
					self.createThread(forumId, title, intropost).done(function(threadId) { 
						deferred.resolve({ threadId: threadId, forumId: forumId });
					});
				}

			});
		}
	};

	return ForumManagement;
});