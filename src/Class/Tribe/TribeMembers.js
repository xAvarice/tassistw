define(['underscore'], function(_) {

	var WorldTribes = {
		store: function(tribes) {
			localStorage.lmfWorldTribes = JSON.stringify(tribes);
			localStorage.lmfWorldTribesAge = Date.now() / 1000;
		},

		getPlayersByField: function(field, value) {
			var players = [];
			var stored = this.getFromStorage();
			var select = {};
			select[field] = value;

			return _.where(stored, select);
		},

		getPlayersByTribeIds: function(tribeIds) {
			var method = function(player) { 
				var testResult = _.contains(tribeIds, player.tribeId);
				return testResult;
			};
			var players = this.getFromStorage();
			var result = _.filter(players, method);
			return result;
		},

		// Return age of store in minutes
		getStorageAge: function() {
			var age = false;

			if (localStorage.lmfWorldTribes) {
				var now = Date.now() / 1000;
				age = (now - localStorage.lmfWorldTribesAge) / 60;
			}

			return age;
		},
		parseMemberData: function(data) {
			var rows = data.split('\n');
			var result = [];

			for (var i = 0; i < rows.length; i++) {
				var columns = rows[i].split(',');
				var tribeMember = {
					id: columns[0], // 840754
					name: columns[1], // enepling
					tribeId: columns[2], // 181
					villageCount: columns[3], // 166
					points: columns[4], // 1538724
					rank: columns[5], // 8
				};

				result.push(tribeMember);
			}

			return result;
		},
		load: function() {
			var self = this;
			return $.Deferred(function() {
				var tribes = self.getFromStorage();
				var deferred = this;

				if (tribes) {
					this.resolve(self);

				} else {
					self.getFresh().done(function(response) {
						tribes = self.parseMemberData(response);
						self.store(tribes);
						deferred.resolve(self);
					});
				}
			});
		},
		clean: function() {
			delete localStorage.lmfWorldTribes;
			delete localStorage.lmfWorldTribesAge;
		},
		getFromStorage: function() {
			var age = this.getStorageAge()
			if (age && age < 10) {
				return JSON.parse(localStorage.lmfWorldTribes);
			} else {
				this.clean();
			}

			return false;
		},
		getFresh: function() {
			return $.ajax({
				url: '/map/tribe.txt'
			});
		}
	};

	return WorldTribes;
});