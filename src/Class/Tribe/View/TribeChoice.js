define(['underscore', 'text!Template/Tribe/tribe-choice.html'],
	function(_, tribeChoiceTmpl) {
			
	var TribeChoiceHandler = function(message = '', prefkey = '') {
		this.choices = [];

		// Pref key ensures preferences are handler based.
		this.storageKey = 'lmfTribeChoicePref' + prefkey;

		this.addChoice = function(choice) {

			// TO DO: Validate not already added.

			this.storeChoice(choice);
			$('#tribe-choice-choices').append(this.generateRow(choice));

			// Enable inputs when finished
			this.enable();
		}

		this.generateRow = function(choice) {
			return '<tr>' +
					'<td>' + choice.rank + '</td>' +
					'<td>' + choice.name + '</td>' +
					'<td>' + choice.players + '</td>' +
					'<td>' + choice.villages + '</td>' +
				'</tr>';
		}

		this.getChoiceIds = function() {
			var choices = this.getStoredChoices();
			return _.pluck(choices, '')
		}

		this.getStoredChoices = function() {
			var pref = window.localStorage[this.storageKey];

			if (pref) {
				return JSON.parse(pref);
			}

			return [];
		}

		this.storeChoice = function(choice) {
			this.storeChoices([choice]);
		}

		this.storeChoices = function(choices) {
			choices = $.merge(this.getStoredChoices(), choices);
			window.localStorage[this.storageKey] = JSON.stringify(choices);
			return this;
		}

		this.getTribeIdFromRankingRow = function(row) {
			var href = $(row).eq(1).children().eq(0).attr('href');
			var id = href.match(/&id=\d+/)[0].split('&id=')[1];
			return id;
		}

		this.parseChoiceFromRow = function(row) {
			var choice = {
				rank: $(row).eq(0).text(),
				name: $(row).eq(1).text().trim(),
				players: $(row).eq(4).text(),
				villages: $(row).eq(6).text(),
				id: this.getTribeIdFromRankingRow(row)
			};

			return choice;
		}

		this.disable = function() {
			$('#tribe-choice-form-add').prop('disabled', 'disabled');
			$('#tribe-choice-form-done').prop('disabled', 'disabled');
		}

		this.enable = function() {
			$('#tribe-choice-form-add').removeProp('disabled');
			$('#tribe-choice-form-done').removeProp('disabled');
		}

		this.addSubmitHandler = function() {
			var self = this;
			var handler =  function(event) {
				event.preventDefault();
				var value = $('#tribe-choice-form-input').val();

				$.ajax({
					url: '/game.php?screen=ranking&mode=ally&name=' + value
				}).done(function(result) {

					var html = $(result).find('#ally_ranking_table tr').eq(1).find('.lit-item');
					var choice = self.parseChoiceFromRow(html);
					
					self.disable();
					self.addChoice(choice);
				});

			};

			$('#tribe-choice-form-add').on('click', handler);
			$('#tribe-choice-form').on('submit', handler);
		}


		// add remove choice & unstore methods

		this.complete = function() {
			var choices = this.getStoredChoices();
			window.Dialog.close();
			this.promise.resolve(choices);
		}

		this.addCompletedHandler = function() {
			var self = this;
			$('#tribe-choice-form-done').on('click', function(event) {
				self.complete();
			});
		}

		this.promise = null,

		this.getTribeChoice = function() {
			var self = this;
			return $.Deferred(function() {

				// Get Input Tribes
				var choiceForm = _.template(tribeChoiceTmpl);
				Dialog.show('tribeChoice', choiceForm({ message: message, choices: self.getStoredChoices() }));

				self.addSubmitHandler();
				self.addCompletedHandler();

				// Passed to parent so user can resolve when done
				self.promise = this;
			});
		}
	};

	return TribeChoiceHandler;
});