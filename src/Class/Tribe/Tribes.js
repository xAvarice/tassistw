define(['Class/World/VillageCollection'], function(WorldVillages) {

	var WorldTribes = {
		store: function(tribes) {
			localStorage.lmfWorldTribes = JSON.stringify(tribes);
			localStorage.lmfWorldTribesAge = Date.now() / 1000;
		},
		// Return age of store in minutes
		getStorageAge: function() {
			var age = false;

			if (localStorage.lmfWorldTribes) {
				var now = Date.now() / 1000;
				age = (now - localStorage.lmfWorldTribesAge) / 60;
			}

			return age;
		},
		addVillagesToGroup: function(villageIds, groupId) {
			return $.Deferred(function() {
				var self = this;

				var params = {
					village: game_data.village.id,
					screen: 'overview_villages',
					action: 'bulk_edit_villages',
					mode: 'groups',
					type: 'static',
					partial: '',
					h: window.csrf_token
				};

				var formatted = {
					selected_group: groupId,
					add_to_group: 'Add'
				};
				formatted['village_ids[]'] = []
				formatted['village_ids[]'] = $.merge(formatted['village_ids[]'], villageIds);

				$.ajax({
					method: 'POST',
					url: '/game.php?' + $.param(params),
					data: formatted
				}).done(function() {
					self.resolve(true);
				});

			});
		},
		parseVillageData: function(data) {
			var rows = data.split('\n');
			var result = [];

			for (var i = 0; i < rows.length; i++) {
				var columns = rows[i].split(',');
				var village = {
					id: columns[0],
					name: columns[1],
					x: columns[2],
					y: columns[3],
					player: columns[4],
					points: columns[5]
				};

				result.push(village);
			}

			return result;
		},
		load: function() {
			var self = this;
			return $.Deferred(function() {
				var tribes = self.getFromStorage();
				var deferred = this;

				if (tribes) {
					this.resolve(tribes);

				} else {
					self.getFresh().done(function(response) {
						tribes = self.parseVillageData(response);
						self.store(tribes);
						deferred.resolve(tribes);
					});
				}
			});
		},
		clean: function() {
			delete localStorage.lmfWorldTribes;
			delete localStorage.lmfWorldTribesAge;
		},
		getFromStorage: function() {
			var age = this.getStorageAge();
			if (age && age < 10) {
				return JSON.parse(localStorage.lmfWorldTribes);
			} else {
				this.clean();
			}

			return false;
		},
		getFresh: function() {
			return $.ajax({
				url: '/map/ally.txt'
			});
		}
	};

	return WorldTribes;
});