define(
	[
		'underscore', 
		'text!Style/Menu.css', 
		'text!Template/Menu.html', 
	],
	function(_, MenuCss, MenuTpl) {

		var menu = {
			tools: [
				{ 
					key: 'command', 
					name: 'Timed Command', 
					link: 'https://dl.dropboxusercontent.com/s/h3r2ukk6e1o84gp/supporttimed.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Sends a command (support or attack) to land at provided time.'
				},
				{
					key: 'supporter',
					name: 'Supporter',
					link: 'https://www.dropbox.com/s/gt50a5rvj8ouaph/Supporter.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Send support to self or another player.'
				},
				{ 
					key: 'resfill', 
					name: 'ResFiller', 
					link: 'https://dl.dropboxusercontent.com/s/4vvnf0mmd2mpdfm/resourceGrabber.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Fills village warehouse with resource requests to your other villages'
				},
				{ 
					key: 'assistantkey', 
					name: 'FarmPresser', 
					link: 'https://dl.dropboxusercontent.com/s/dv9n0ihd4zfhgqr/farmAssistant.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Automatically presses B on FA until insufficient troops.'
				},
				{ 
					key: 'troopcount', 
					name: 'troopcount', 
					link: 'https://dl.dropboxusercontent.com/s/vyxp7hysweg8w88/countTroops.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Counts units across entire account.'
				},
				{ 
					key: 'pitbuilder', 
					name: 'PitBuilder', 
					link: 'https://dl.dropboxusercontent.com/s/rzkhbym1x2x09xm/pitBuilder.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Adds resource buildings to queue wherever possible so they are completed first.'
				},
				{ 
					key: 'proxmap', 
					name: 'ProximityMapper', 
					link: 'https://dl.dropboxusercontent.com/s/w61by1nl6y89vzv/defencePlanner.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Analyses danger of villages on a tribe-wide basis.'
				},
				{ 
					key: 'defpull', 
					name: 'defpull', 
					link: 'https://dl.dropboxusercontent.com/s/otcmuxj98v73leb/pullDefenceBasic.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Requests units to current village from all villages in current group.'
				},
				{ 
					key: 'groupcoords', 
					name: 'CoordGrouper', 
					link: 'https://dl.dropboxusercontent.com/s/ua2q5k82vzgy9pp/groupCoords.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Adds a list of coordinates to one of your village groups.'
				},
				{ 
					key: 'newnoter', 
					name: 'PlayerNotes', 
					link: 'https://dl.dropboxusercontent.com/s/zwm8nzzbjufviju/enemyNotes.js?dl=0',
					class: 'tool-choice-option',
					imageUrl: '',
					description: 'Collects notes for all of a player\'s villages'
				}
			],

			open: function() {
				var self = this;
				var menuTemplate = _.template(MenuTpl);
				var menuContent = menuTemplate({ tools: self.tools });

				$('head').append('<style>' + MenuCss + '</style>');

				Dialog.show('lmfDialogMenu', menuContent);

				$('#tool-choice-form .tool-choice-option').on('click', function(event) {
					var toolkey = $(event.target).closest('.tool-choice-option').data('type');
					var tool = _.find(self.tools, { key: toolkey });
					$.getScript(tool.link);
					Dialog.close();
				});
			}
		}

		return menu;
	}
)