define(
	['Class/Game/Map', 'Class/Command/AttackSender'], 
	function(GameMap, AttackSender) {

	var Farmer = {
		run: function() {
			console.log('Testing');
			this.findTargets();
			this.farm();
		},
		findTargets: function() {
			var map = GameMap.findVillagesWithin(2, 2);
		},
		farm: function(targets, criteria) {
			AttackSender.send();
		},
		getCriteria: function() {},
		getAttackParams: function() {

		}
	};

	return Farmer;
});